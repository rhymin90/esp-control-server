import gc
import time
from machine import Pin
import network
import socket
# import esp
#
# esp.osdebug(None)
gc.collect()

led = Pin(2, Pin.OUT)

led(0)  # on as server running

ssid=''
password=''

def do_connect():
    ap = network.WLAN(network.AP_IF)  # AP Objekt
    ap.active(False)
    # ap.config(essid='cool-esp', password='password543')
    # while ap.active() == False:
    #     pass

    print('Connection successful')
    print(ap.ifconfig())
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(ssid, password)
        while not sta_if.isconnected():
            led(0)  # led on to show connecting
            pass
    # blink 5 times to show connected
    for i in range(3):
        led(0)  # an
        time.sleep(0.25)
        led(1)  # aus
        time.sleep(0.25)
    print('network config:', sta_if.ifconfig())


do_connect()

pins = [Pin(i, Pin.OUT) for i in (0, 2, 4, 5, 12, 13, 14, 15)]

html = """<!DOCTYPE html>
<html>
    <head>
    <title>ESP8266 Pins</title> 
<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/bulma@1.0.1/css/bulma.min.css"
>
    </head>
    <body> <h1>ESP8266 Pins</h1>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
    </body>
</html>
"""

# addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(3)
# s.bind(addr)
# s.listen(1)

led(1)  # on as server running

print('listening on port 80')

def get_row(p):
    return """<tr>
                    <td>%s</td><td>%s</td>
                    <td><button class="button is-success is-dark" onclick="location.href='?pin=%s&value=0'">%s ON</button></td>
                    <td><button class="button is-danger is-dark" onclick="location.href='?pin=%s&value=1'">%s OFF</button></td>  
                 </tr>""" % (str(p), 'ON' if (p.value() == 0) else 'OFF',  str(p),  str(p),  str(p), str(p))

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    # print('Content = %s' % request)
    for p in pins:
        pin_on = request.find('/?pin=%s&value=1' % str(p))
        pin_off = request.find('/?pin=%s&value=0' % str(p))
        if pin_on == 6:
            p.on()
        if pin_off == 6:
            p.off()

    rows = [(get_row(p)) for p in pins]
    response = html % '\n'.join(rows)
    conn.send('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
    conn.sendall(response)
    conn.close()
    # gc.collect()
